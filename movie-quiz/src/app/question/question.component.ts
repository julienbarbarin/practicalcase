import { Component, Input, OnInit } from '@angular/core';
import axios, { AxiosResponse } from 'axios';

import { Utils } from 'src/utils/utils'
import { Question } from 'src/models/types/question';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  private QUIZ_DURATION_SEC: number = 60;

  currentQuestion: Question | undefined;
  nextQuestion: Question | undefined;
  currentScore: number = 0;

  currentTimeInSec: number = 0;

  isQuizRunning: boolean = true;
  highestScore: number = Utils.GetCookie("highestscore");

  ngOnInit(): void {
    this.startQuiz();
  }

  startQuiz() {
    Utils.getQuestionResponse().then((response) => {
      this.currentQuestion = response;
      this.currentScore = 0;
      this.startTimer();
    });
  }

  startTimer() {
    this.isQuizRunning = true;
    let interval = setInterval(() => {
      if (this.currentTimeInSec >= this.QUIZ_DURATION_SEC || !this.isQuizRunning) {
        this.gameOver();
        clearInterval(interval);
      }
      else {
        this.currentTimeInSec++;
      }
    }, 1000)
  }

  setAnswer(response: boolean) {
    if (this.currentQuestion?.answer == response) {//Good answer ->increase the score
      this.currentScore += 1;
    }
    Utils.getQuestionResponse().then((response) => { this.currentQuestion = response });
  }

  gameOver() {
    this.isQuizRunning = false;
    this.currentTimeInSec = 0;
    this.currentQuestion = undefined;
    // check the highest score
    this.highestScore = Math.max(this.highestScore, this.currentScore);
    Utils.SetCookie("highestscore", this.highestScore, 1000);
  }
}
