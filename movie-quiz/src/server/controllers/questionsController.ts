
import axios, { AxiosResponse } from 'axios';

import { Utils } from '../../utils/utils';
import { Question } from '../../models/types/question';
import { Movie } from '../../models/types/movie';
import { Actor } from '../../models/types/actor';

class questionsController {

    private static API_KEY = "10b48eaf0bbd33ba64e4954b86e87531";

    private static ACTORS_FIRST_PAGES_NUMBER = 15;
    private static MOVIES_FIRST_PAGES_NUMBER = 15;

    /** Return an actor with the list of its movies */
    async getActorWithMovie() {
        let result: Actor = new Actor();

        let randomPage = Math.ceil((Math.random() * questionsController.ACTORS_FIRST_PAGES_NUMBER));
        do {
            await axios.get(`https://api.themoviedb.org/3/person/popular?api_key=${questionsController.API_KEY}&language=en-US&page=${randomPage}`)
                .then(response => {
                    if (response.data.results) {
                        //Get a random actor
                        result = Utils.GetRandomItem(response.data.results) as Actor;
                    }
                })
                .catch((error) => {
                    console.log("getActorWithMovie error : ", error);
                })
        } while (!result || (result && result.known_for_department !== 'Acting')) // We want an actor

        //Keep only movies (not tv)
        result.known_for = result?.known_for?.filter((kf: Movie) => {
            return kf.media_type === 'movie';
        })

        return result;
    }

    /** Return a random movie */
    async getMovie(moviesIdsToExclude: number[] | undefined) {
        let result: Movie = new Movie();

        let randomPage = Math.ceil((Math.random() * questionsController.MOVIES_FIRST_PAGES_NUMBER));

        do {
            await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=${questionsController.API_KEY}&language=en-US&page=${randomPage}`)
                .then(response => {
                    if (response.data.results) {
                        //Get a random movie
                        result = Utils.GetRandomItem(response.data.results) as Movie;
                    }
                })
                .catch((error) => {
                    console.log("getMovie error : ", error);
                })
        } while (!result || !result.id || (result && result.id && moviesIdsToExclude?.includes(result.id))) // we want a movie where the selected actor didn't star

        return result;
    }

    /** Return an actor, a movie and the answer */
    async getQuestion() {
        let result: Question;

        //Get an actor
        let actor = await this.getActorWithMovie();

        //Choose if the answer will be 'Yes' or 'No'
        let answer = Math.random() <= 0.5;

        let movie;

        if (answer && actor && actor.known_for && actor.known_for.length > 0) {
            movie = Utils.GetRandomItem(actor.known_for) as Movie;
        }
        else {
            answer = false; // Force the answer to false in the case where the given actor has no movie

            //Get the ids of the actor's movies
            let moviesIdsToExclude = actor.known_for?.map((kf: Movie) => {
                return kf.id ? kf.id as number : 0;
            });

            //Get an movie
            movie = await this.getMovie(moviesIdsToExclude);
        }

        return new Question(actor.name, actor.profile_path, movie?.title, movie?.poster_path, answer);
    }
}

export = new questionsController();
