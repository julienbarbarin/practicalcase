
import { NextFunction, Request, Response, Router } from 'express';
import questionsController from '../../../controllers/questionsController';
// import {movieQuery } from '../../../models/Types'

class questionsRouter {
  private _router = Router();
  private _controller = questionsController;

  get router() {
    return this._router;
  }

  constructor() {
    this._configure();
  }

  /**
   * Connect routes to their matching controller endpoints.
   */
  private _configure() {
    this._router.get('/', async (req: Request, res: Response, next: NextFunction) => {

      const result = await this._controller.getQuestion();
      
      res.status(200).json(result);
    });
  }
}

export = new questionsRouter().router;