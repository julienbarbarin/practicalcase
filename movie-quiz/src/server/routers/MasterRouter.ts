import { Router } from 'express';
import questionsRouter from './routers/questions/questionsRouter';

class MasterRouter {
  private _router = Router();
  private _questionsRouter = questionsRouter;

  get router() {
    return this._router;
  }

  constructor() {
    this._configure();
  }

  /**
   * Connect routes to their matching routers.
   */
  private _configure() {
    this._router.use('/question', this._questionsRouter);
  }
}

export = new MasterRouter().router;