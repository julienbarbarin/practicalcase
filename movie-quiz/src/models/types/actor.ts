import { Movie } from "./movie";

export class Actor {

    constructor(id?: number, known_for?: Movie[], name?: string, known_for_department?: string, profile_path?: string) {
        this.id = id;
        this.known_for = known_for;
        this.name = name;
        this.known_for_department = known_for_department;
        this.profile_path = profile_path;
    }

    id?: number;
    known_for?: Movie[];
    name?: string;
    known_for_department?: string;
    profile_path?: string;
}