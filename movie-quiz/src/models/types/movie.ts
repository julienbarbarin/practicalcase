export class Movie {

    constructor() { }

    poster_path?: string;
    id?: number;
    media_type?: string;
    original_title?: string;
    original_language?: string;
    title?: string;
    backdrop_path?: string;
}