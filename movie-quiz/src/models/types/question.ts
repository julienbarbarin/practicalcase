export class Question {
    actorName: string | undefined
    actorPictureFullPath: string = ""
    movieTitle: string | undefined
    moviePictureFullPath: string = ""
    answer: boolean

    constructor(actorName: string | undefined, actorPicturePath: string | undefined, movieTitle: string | undefined, moviePicturePath: string | undefined, answer: boolean) {
        this.actorName = actorName;
        this.actorPictureFullPath = `http://image.tmdb.org/t/p/w185${actorPicturePath}`;
        this.movieTitle = movieTitle;
        this.moviePictureFullPath = `http://image.tmdb.org/t/p/w342/${moviePicturePath}`
        this.answer = answer;
    }

}