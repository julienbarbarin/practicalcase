import axios, { AxiosResponse } from 'axios';
export class Utils {
    static GetRandomItem(array: any[]): any {
        let randomIndex = Math.floor(Math.random() * (array.length - 1));
        return array[randomIndex];
    }

    static async getQuestionResponse() {
        console.log("getQuestionResponse");

        let result;

        await axios.get("http://localhost:5000/api/question")
            .then(response => {
                result = response.data;
                console.log("Question : ", result);
            })
            .catch((error) => {
                console.log("getQuestionResponse error : ", error);
            })

        return result;
    }

    static SetCookie(name: string, value: number, days: number) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    static GetCookie(name: string) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return Number.parseInt(c.substring(nameEQ.length, c.length));
        }
        return 0;
    }
}