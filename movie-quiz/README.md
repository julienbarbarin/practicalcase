# MovieQuiz

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.5.

## Structure

The project provides front end and back end of the movie quiz app.

The back end source files are stored in the `src\server` folder.

The front end source files are stored in the `src\app` folder.

## Development servers

At the first time, run `npm install` to install all dependencies.

To run all the app, run `npm run full`. Navigate to `http://localhost:4200/` to use the app. Navigate to `http://localhost:5000/api/question` to get a random question from the back end.

NOTE : On Windows, run `npm run back` in one terminal and `npm run front` in another one to run the app.